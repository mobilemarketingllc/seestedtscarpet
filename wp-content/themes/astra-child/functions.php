<?php
/**
 * astra-child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package astra-child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    //wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
 function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

 //Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );
//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumb() {
    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
    }
}
add_action( 'astra_header_after', 'bbtheme_yoast_breadcrumb' );
function new_year_number()
{
return $new_year = date('Y');
}
add_shortcode('year_code', 'new_year_number');

//Covid home page banner hook
function bbtheme_covid_banner_custom() {
    $iscovid  =  get_option('covid');
     if ( $iscovid == '1' &&  is_front_page() ) {

          $content = do_shortcode('[fl_builder_insert_layout slug="covid19-banner-row"]');
        
     }
     echo $content;
}
add_action( 'astra_header_after', 'bbtheme_covid_banner_custom' );


function mmsession_custom_footer_js_astratheme() {
     
    echo "<script src='https://session.mm-api.agency/js/mmsession.js' async></script>";
}
add_action( 'wp_footer', 'mmsession_custom_footer_js_astratheme' );