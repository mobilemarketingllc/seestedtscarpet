<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 
	
	if(strpos($image , 's7.shawimg.com') !== false){
		if(strpos($image , 'http') === false){ 
		$image = "http://" . $image;
	}	
		$image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
	}else{
		if(strpos($image , 'http') === false){ 
		$image = "https://" . $image;
	}	
		$image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
	}	
?>


<?php if(productdetail_layout == 'box'){ ?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">
	<div class="fl-post-content clearfix grey-back" itemprop="text">
        <div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 product-swatch">   
					<?php 
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
						include( $dir );
					?>	
				</div>
            </div>
            <div class="col-md-6 col-sm-6 product-box">

                <?php //get_template_part('includes/product-brand-logos'); 
										
                $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
				include_once( $dir );
                
                ?>
                <?php if(get_field('parent_collection')) { ?>
                <h4><?php the_field('parent_collection'); ?> </h4>
                <?php } ?>
              
                <h1 class="fl-post-title" itemprop="name">
                   <?php the_field('style'); ?>
                </h1>
				 <h2 class="fl-post-title" itemprop="name">
                   <?php the_field('color'); ?>
                </h2>
				 <!--<h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5> -->
				

                <div class="product-colors">
                    <?php
                    $familysku = get_post_meta($post->ID, 'collection', true);                  

                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => 'collection',
                                'value'   => $familysku,
                                'compare' => '='
                            )
                        )
                    );
                    ?>
                    <?php
                    $the_query = new WP_Query( $args );
                    ?>
                    <ul>
                        <li class="found"><?php  echo $the_query ->found_posts; ?></li>


                        <li class="colors">Colors<br/>Available</li>
                    </ul>

                </div>

                <a href="/flooring-coupon/" class="fl-button" role="button" style="width: auto;">
                    <span class="fl-button-text">GET COUPON</span>
                </a>
<br />
                <a href="/contact-us/" class="fl-button btn-white" role="button" style="width: auto;">
                    <span class="fl-button-text">CONTACT US</span>
                </a>
                <br />
                <a href="/flooring-financing/">GET FINANCING ></a>
				<br />
				<br />
                <a href="/schedule-appointment/">SCHEDULE A MEASUREMENT ></a>
                <div class="product-atts"></div>
            </div>
        </div>


        <?php //get_template_part('includes/product-color-slider'); 
        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider.php';
        include( $dir );
        ?>
</div>



    </div><!-- .fl-post-content -->
<div class="container">
    <?php //get_template_part('includes/product-attributes');
    $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
    include( $dir );
    ?>
</div>

	<?php //FLTheme::post_bottom_meta(); ?>
	<?php //FLTheme::post_navigation(); ?>
	<?php //comments_template(); ?>

</article>

                <?php } ?>
<!-- .fl-post -->


<?php if(productdetail_layout == 'fullwidth'){ 
  $brand = get_field('brand') ;  
?>

<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">
	<div class="fl-post-content clearfix " itemprop="text">
        <?php //get_template_part('includes/product-color-slider');  
        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider-full.php';
        include( $dir );
        ?>
		<div class="productInfo row productDesktop">
			<div class="container">
				<div class="colorTxt">Color</div>
				<h2 class="colorName"><?php echo get_field('color');?></h2>
				<div class="col-md-3 col-sm-3">
					<div class="brandLogo">
						<?php

						if (get_field('brand') != ''){  echo get_field('brand'); }?>


					</div>
					<div class="requestQuote">
						
						<a href="/flooring-coupon/?keyword=<?php echo $_COOKIE['keyword']; ?>&brand=<?php echo $brand;?>">GET COUPON</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/flooring-financing/">GET FINANCING</a>        
					</div>

                </div>
 
				<div class="col-md-6 col-sm-6 product-swatch">
					<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;background-position:bottom">
						<img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" />
					</div>

					<?php if(get_field('room_scene_image_link')){ ?>
					<div class="toggle-image-thumbnails">
						<?php

	foreach($a as $k=>$v){
						?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a><?php
	}
						?>
					</div>
					<?php } ?>





				</div>
				<div class="col-md-3 col-sm-3">
                    <?php //get_template_part('includes/product-attributes'); 
                    $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
                    include( $dir );
                    ?>
				</div>

			</div>
		</div>

		<div class="productInfo row productMobile">
			<div class="colorTxt">Color</div>
			<h2 class="colorName"><?php echo get_field('color');?></h2>
			<div class="productImage">
				<div class="productImg" style="position:relative;">


					<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;background-position:bottom">
						<img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" />
					</div>

					<?php if(get_field('room_scene_image_link')){ ?>
					<div class="toggle-image-thumbnails">
						<?php
	foreach($a as $k=>$v){
						?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover; margin-top:10px;" title="<?php the_title_attribute(); ?>"></a><?php
	}
						?>
					</div>
					<?php } ?>

				</div>
				<div class="col-md-3 col-sm-3">
					<div class="brandLogo" style="text-align:center;">
						<?php if (get_field('brand') != ''){  echo get_field('brand'); }?>


					</div>
					<div class="requestQuote">
						<a href="/request-quote/">REQUEST A QUOTE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/flooring-coupon/?keyword=<?php echo $_COOKIE['keyword']; ?>&brand=<?php echo $brand;?>">GET COUPON</a>    

					</div>

				</div>

				<div class="col-md-3 col-sm-3">
					<div style="height:50px; clear:both;">&nbsp;</div>
                    <?php //get_template_part('includes/product-attributes');
                    $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
                    include( $dir );
                    ?>      
				</div>


			</div>
			<?php //get_template_part('includes/product-attributes'); ?>
			<?php //get_template_part('includes/product-extra-info');   ?>
		</div>
        </article>
        
        
<?php } ?>