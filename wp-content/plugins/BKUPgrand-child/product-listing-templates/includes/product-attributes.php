<div class="product-attributes">
    <h3>Product Attributes</h3>
    <table class="table">
        <thead>
        <tbody>

        <?php if(get_field('parent_collection')) { ?>
            <tr>
                <th scope="row">Main Collection</th>
                <td><?php the_field('parent_collection'); ?></td>
            </tr>
        <?php } ?>

        <?php if(get_field('collection')) { ?>
            <tr>
                <th scope="row">Collection</th>
                <td><?php the_field('collection'); ?></td>
            </tr>
        <?php } ?>

        <?php if(get_field('color')) { ?>
            <tr>
                <th scope="row">Color</th>
                <td><?php the_field('color'); ?></td>
            </tr>
        <?php } ?>

 <?php if(get_field('construction')) { ?>
            <tr>
                <th scope="row">Construction</th>
                <td><?php the_field('construction'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('color_variation')) { ?>
         <tr>
            <th scope="row">Color Variation</th>
            <td><?php the_field('color_variation'); ?></td>
        </tr>
        <?php } ?>
        
        <?php if(get_field('shade')) { ?>
            <tr>
                <th scope="row">Shade</th>
                <td><?php the_field('shade'); ?></td>
            </tr>
        <?php } ?>
			 <?php if(get_field('shape')) { ?>
            <tr>
                <th scope="row">Shape</th>
                <td><?php the_field('shape'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('species')) { ?>
        <tr>
            <th scope="row">Species</th>
            <td><?php the_field('species'); ?></td>
        </tr>
        <?php } ?>
        <?php if(get_field('fiber_type')) { ?>
            <tr>
                <th scope="row">Fiber Type</th>
                <td><?php the_field('fiber_type'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('surface_type')) { ?>
            <tr>
                <th scope="row">Surface Type</th>
                <td><?php the_field('surface_type'); ?></td>
            </tr>
        <?php } ?>
			<?php if(get_field('surface_texture_facet')) { ?>
            <tr>
                <th scope="row">Surface Texture</th>
                <td><?php the_field('surface_texture_facet'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('style')) { ?>
            <tr>
                <th scope="row">Style</th>
                <td><?php the_field('style'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('edge')) { ?>
            <tr>
                <th scope="row">Edge</th>
                <td><?php the_field('edge'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('application')) { ?>
            <tr>
                <th scope="row">Application</th>
                <td><?php the_field('application'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('size')) { ?>
            <tr>
                <th scope="row">Size</th>
                <td><?php the_field('size'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('width')) { ?>
            <tr>
                <th scope="row">Width</th>
                <td><?php the_field('width'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('length')) { ?>
            <tr>
                <th scope="row">Length</th>
                <td><?php the_field('length'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('thickness')) { ?>
            <tr>
                <th scope="row">Thickness</th>
                <td><?php the_field('thickness'); ?></td>
            </tr>
        <?php } ?>
       
        <?php if(get_field('installation')) { ?>
            <tr>
                <th scope="row">Installation Method</th>
                <td><?php the_field('installation'); ?></td>
            </tr>
        <?php } ?>
			
			<?php if(get_field('location')) { ?>
            <tr>
                <th scope="row">Location</th>
                <td><?php the_field('location'); ?></td>
            </tr>
        <?php } ?>
			<?php if(get_field('backing_facet')) { ?>
            <tr>
                <th scope="row">Backing</th>
                <td><?php the_field('backing_facet'); ?></td>
            </tr>
        <?php } ?>
			<?php if(get_field('look')) { ?>
            <tr>
                <th scope="row">Look</th>
                <td><?php the_field('look'); ?></td>
            </tr>
        <?php } ?>
			
        </tbody>
    </table>
    </div>