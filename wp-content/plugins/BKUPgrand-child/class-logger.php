<?php

trait WP_Example_Logger {

	/**
	 * Really long running process
	 *
	 * @return int
	 */
	public function really_long_running_task() {
		return sleep( 1 );
	}

	/**
	 * Log
	 *
	 * @param string $message
	 */
	public function log( $message ) {
		error_log( $message );
	}

	/**
	 * Get lorem
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	protected function get_message( $name,$id ) {

		$msg = $name.'-'.$id.' Inserted Successfully' ;
		
		return $msg;
	}

	/**
	 * Insert Product 
	 *
	 * @param string $message
	 */
	 public function insert_product( $data ,$main_category) {

		if($data['status']=='active'){

			write_log($data['post_type']);

			// args for checking already inserted product

			$args = array(
				'numberposts'	=> -1,
				'post_type'		=> $data['post_type'],
				'meta_key'		=> 'sku',
				'meta_value'	=> $data['sku']
			);
			
			// query
			$the_query = new WP_Query( $args );

		 if( $the_query->have_posts() ){

			while ( $the_query->have_posts() ) : $the_query->the_post();
				
					$post_id = get_the_ID();
					write_log('Already-'.$post_id);
					
		 endwhile;

		 }else{			

		// Insert post data.

			$my_post = array(
				'post_title'    => $data['name'].' '.$data['sku'],
				'post_content'  => '',
				'post_type'  => $data['post_type'],
				'post_status'   => 'publish',
				'post_author'   => 1,				
			);

			
			
			// Insert the post into the database.
			$post_id = wp_insert_post( $my_post );
			write_log('New-'.$post_id);

		}

			
		 if(isset($data['featured'])){
			update_post_meta( $post_id, 'featured', $data['featured'] ); //featured
		  }
		  if(isset($data['in_stock'])){
			update_post_meta( $post_id, 'in_stock', $data['in_stock'] ); //in stock
		  }
		  if(isset($data['application'])){
			update_post_meta( $post_id, 'application', $data['application'] ); //application
		  }
		  if(isset($data['application_facet'])){
			update_post_meta( $post_id, 'application_facet', $data['application_facet'] ); //application_facet
		  }
		  if(isset($data['backing_facet'])){
			update_post_meta( $post_id, 'backing_facet', $data['backing_facet'] ); //backing_facet
		  }
		  if(isset($data['brand'])){
			update_post_meta( $post_id, 'brand', $data['brand'] ); //brand
		  }
		  if(isset($data['brand_facet'])){
			update_post_meta( $post_id, 'brand_facet', $data['brand_facet'] ); //brand_facet 
		  }
		  if(isset($data['parent_collection'])){
			update_post_meta( $post_id, 'parent_collection', $data['parent_collection'] ); //parent_collection
		  }
		  if(isset($data['collection_name'])){
			update_post_meta( $post_id, 'collection', $data['collection_name'] ); //collection
		  }
		  if(isset($data['collection_facet'])){
			update_post_meta( $post_id, 'collection_facet', $data['collection_facet'] ); //collection_facet
		  }
		  if(isset($data['color'])){
			update_post_meta( $post_id, 'color', $data['color'] ); //color
		  }
		  if(isset($data['color_code'])){
			update_post_meta( $post_id, 'color_code', $data['color_code'] ); //color_code
		  }
		  if(isset($data['color_facet'])){
			update_post_meta( $post_id, 'color_facet', $data['color_facet'] ); //color_facet
		  }
		  if(isset($data['color_variation'])){
			update_post_meta( $post_id, 'color_variation', $data['color_variation'] ); //color_variation
		  }
		  if(isset($data['construction'])){
			update_post_meta( $post_id, 'construction', $data['construction'] ); //construction
		  }
		  if(isset($data['in_stock'])){
			update_post_meta( $post_id, 'construction_facet', $data['construction_facet'] ); //construction_facet 
		  }
		  if(isset($data['edge'])){
			update_post_meta( $post_id, 'edge', $data['edge'] ); //edge
		  }
		  if(isset($data['installation_facet'])){
			update_post_meta( $post_id, 'installation_facet', $data['installation_facet'] ); //installation_facet
		  }
		  if(isset($data['installation'])){
			update_post_meta( $post_id, 'installation_method', $data['installation'] ); //installation_method
		  }
		  if(isset($data['installation_location'])){
			update_post_meta( $post_id, 'installation_location', $data['installation_location'] ); //installation_location
		  }
		  if(isset($data['length'])){
			update_post_meta( $post_id, 'length', $data['length'] ); //length
		  }
		  if(isset($data['lifestyle'])){
			update_post_meta( $post_id, 'lifestyle', $data['lifestyle'] ); //lifestyle
		  }
		  if(isset($data['lifestyle_facet'])){
			update_post_meta( $post_id, 'lifestyle_facet', $data['lifestyle_facet'] ); //lifestyle_facet
		  }
		  if(isset($data['location'])){
			update_post_meta( $post_id, 'location', $data['location'] ); //location
		  }
		  if(isset($data['location_facet'])){
			update_post_meta( $post_id, 'location_facet', $data['location_facet'] ); //location_facet
		  }
		  if(isset($data['look'])){
			update_post_meta( $post_id, 'look', $data['look'] ); //look
		  }
		  if(isset($data['look_facet'])){
			update_post_meta( $post_id, 'look_facet', $data['look_facet'] ); //look_facet
		  }
		  if(isset($data['made_in_the_usa'])){
			update_post_meta( $post_id, 'made_in_the_usa', $data['made_in_the_usa'] ); //made_in_the_usa
		  }
		  if(isset($data['manufacturer'])){
			update_post_meta( $post_id, 'manufacturer', $data['manufacturer'] ); //manufacturer
		  }
		  if(isset($data['material'])){
			update_post_meta( $post_id, 'material', $data['material'] ); //material
		  }
		  if(isset($data['material_facet'])){
			update_post_meta( $post_id, 'material_facet', $data['material_facet'] ); //material_facet
		  }
		  if(isset($data['shade'])){
			update_post_meta( $post_id, 'shade', $data['shade'] ); //shade
		  }
		  if(isset($data['shade_facet'])){
			update_post_meta( $post_id, 'shade_facet', $data['shade_facet'] ); //shade_facet
		  }
		  if(isset($data['shape'])){
			update_post_meta( $post_id, 'shape', $data['shape'] ); //shape
		  }
		  if(isset($data['shape_facet'])){
			update_post_meta( $post_id, 'shape_facet', $data['shape_facet'] ); //shape_facet
		  }
		  if(isset($data['size'])){
			update_post_meta( $post_id, 'size', $data['size'] ); //size
		  }
		  if(isset($data['sku'])){
			update_post_meta( $post_id, 'sku', $data['sku'] ); //sku
		  }
		  if(isset($data['sku_display'])){
			update_post_meta( $post_id, 'sku_display', $data['sku_display'] ); //sku_display
		  }
		  if(isset($data['species'])){
			update_post_meta( $post_id, 'species', $data['species'] ); //species
		  }
		  if(isset($data['species_facet'])){
			update_post_meta( $post_id, 'species_facet', $data['species_facet'] ); //species_facet
		  }
		  if(isset($data['style'])){
			update_post_meta( $post_id, 'style', $data['style'] ); //style
		  }
		  if(isset($data['style_facet'])){
			update_post_meta( $post_id, 'style_facet', $data['style_facet'] ); //style_facet
		  }
		  if(isset($data['style_code'])){
			update_post_meta( $post_id, 'style_code', $data['style_code'] ); //style_code
		  }
		  if(isset($data['surface_texture_facet'])){
			update_post_meta( $post_id, 'surface_texture_facet', $data['surface_texture_facet'] ); //surface_texture_facet
		  }
		  if(isset($data['surface_type'])){
			update_post_meta( $post_id, 'surface_type', $data['surface_type'] ); //surface_type
		  }
		  if(isset($data['thickness'])){
			update_post_meta( $post_id, 'thickness', $data['thickness'] ); //thickness
		  }
		  if(isset($data['thickness_facet'])){
			update_post_meta( $post_id, 'thickness_facet', $data['thickness_facet'] ); //thickness_facet
		  }
		  if(isset($data['warranty_text'])){
			update_post_meta( $post_id, 'warranty_info', $data['warranty_text'] ); //warranty_info
		  }
		  if(isset($data['width'])){
			update_post_meta( $post_id, 'width', $data['width'] ); //width
		  }
		  if(isset($data['width_facet'])){
			update_post_meta( $post_id, 'width_facet', $data['width_facet'] ); //width_facet
		  }
		  if(isset($data['swatch'])){
			update_post_meta( $post_id, 'swatch_image_link', $data['swatch'] ); //swatch_image_link
		  }
		  if(isset($data['fiber_brand'])){
			update_post_meta( $post_id, 'fiber_brand', $data['fiber_brand'] ); //fiber_brand
		  }
		  if(isset($data['fiber_type'])){
			update_post_meta( $post_id, 'fiber_type', $data['fiber_type'] ); //warranty_info
			}		
			if(isset($data['gallery_images'])){
				update_post_meta( $post_id, 'gallery_room_images', $data['gallery_images'] ); //gallery_room_images
			}
			
			

			 return $post_id;
		
	}
	 }

}