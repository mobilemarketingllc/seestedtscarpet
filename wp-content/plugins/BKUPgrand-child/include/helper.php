<?php

//Tranding Product ShortCode

function shopofarearug($arg){
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-colors/',
            'image_url'=>'https://wpmaster.mm-dev.agency/wp-content/uploads/2018/03/Color.png',
            'title'=> "Shop by Color"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-styles/',
            'image_url'=>'https://wpmaster.mm-dev.agency/wp-content/uploads/2018/03/style.png',
            'title'=> "Shop by Style"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-sizes/',
            'image_url'=>'https://wpmaster.mm-dev.agency/wp-content/uploads/2018/03/Size.png',
            'title'=> "Shop by Size"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-patterns/',
            'image_url'=>'https://wpmaster.mm-dev.agency/wp-content/uploads/2018/03/style-copy.png',
            'title'=> "Shop by Patterns"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-brands/',
            'image_url'=>'https://wpmaster.mm-dev.agency/wp-content/uploads/2018/03/brands.png',
            'title'=> "Shop by Brands"
        ),
        array(
            'url'=>'https://rugs.shop/on-sale/',
            'image_url'=>'https://wpmaster.mm-dev.agency/wp-content/uploads/2018/03/sale.png',
            'title'=> "Area Rug Sale"
        ),
       
    );
    $result ="";
    $result.= '<div class="products-list column-3"><ul class="product-list">';
    for($i=0;$i<count($data);$i++){
        $result .='<li class="product-li-three-row">
                    <div class="product-inner">
                        <div class="product-img-holder">
                            <a href="'.$data[$i]['url'].'" target="_blank" itemprop="url" class="">
                                <img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" />
                            </a>
                        </div>
                        <h3 class="fl-callout-title"><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h3>
                    </div>
                </li>';
    }
    $result.= '</ul></div>';
   return $result;
   
                           
}

add_shortcode( 'shoparearug', 'shopofarearug' );

//Tranding Product ShortCode

function arearugProductList($arg){
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/',
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'99.00',
            'title'=> "PETRA MULTI",
            'collection' => 'Spice Market',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/',
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'499.00',
            'title'=> "Esperance Seaglass",
            'collection' => 'Titanium Collection',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/',
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'156.00',
            'title'=> "Nirvana Indigo",
            'collection' => 'Cosmopolitan',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/',
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'62.40',
            'title'=> "7139a",
            'collection' => 'Andora',
            'brand' => 'Oriental Weavers'
        )
    );
    $result ="";
    $result.= '<div class="products-list"><ul class="product-list">';
    for($i=0;$i<count($data);$i++){
        $result .='<li class="product-li">
            <div class="product-inner">
                <div class="product-img-holder">
                    <a href="'.$data[$i]['url'].'" target="_blank"><img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" /></a>
                </div>
                <div class="product-info">
                    <h6>'.$data[$i]['collection'].'</h6>
                    <h4><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h4>

                    <div class="price-section">
                        <span>Starting at</span>
                        <span class="price"> $'.$data[$i]['price'].'</span>
                        <span class="sale">On Sale</span>
                    </div>

                    <div class="button-section">
                        <a href="'.$data[$i]['url'].'" class="button" target="_blank">Buy Now</a>
                        <div class="brand-wrap">By '.$data[$i]['brand'].'</div>
                    </div>
                </div>
            </div>
        </li>';
    }
    $result.= '</ul></div>';
   return $result;
   
                           
}

add_shortcode( 'area_rug_trading_products', 'arearugProductList' );



function contact_locationlist($arg){
return '<div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
            <div class="">
                <div class="fl-icon-wrap">
                    <div id="fl-icon-text-5be49d131d734" class="fl-icon-text">
                        <a href="'.get_option('api_contact_url_sandbox').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">
                            <p>'.get_option('api_contact_address_sandbox').'</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>';
        
}
add_shortcode( 'contactLocation', 'contact_locationlist' );

function storelocation_address($arg){
    
    $website = json_decode(get_option('website_json'));
    $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
    $locations = "<ul class='storename'>";
    $locations .= '<li>';
    
    for($i=0;$i<count($website->locations);$i++){

        $location_name = isset($website->locations[$i]->name)?$website->locations[$i]->name:"";

        //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
        $location_address  = isset($website->locations[$i]->address)?$website->locations[$i]->address:"";
        $location_address .= isset($website->locations[$i]->city)?" , ".$website->locations[$i]->city:"";
        $location_address .= isset($website->locations[$i]->state)?" , ".$website->locations[$i]->state:"";
        $location_address .= isset($website->locations[$i]->postalCode)?" , ".$website->locations[$i]->postalCode:"";
        
        $location_phone = isset($website->locations[$i]->phone)?$website->locations[$i]->phone:"";


        if(in_array("alldata",$arg)){
                $locations .= '<div class="store-container">';
                //$locations .= '<div class="name">'.$location_name.'</div>';
                $locations .= '<div class="address"><a href=https://www.google.com/maps/dir//'.urlencode($location_address).'" target="_blank" '.'>'.$location_address.'</div>';
                $locations .= '<div class="phone"><a href=tel:'.$location_phone.'>'.'<span>'.$location_phone.'</span></a></div>';
            $locations .= '</div>';

            $locations .= '<div class="store-opening-hrs-container">';
            $locations .= '<ul class="store-opening-hrs">';
                for($j = 0; $j < count($weekdays); $j++) {

                $location .= $website->locations[$i]->monday;
                if(isset($website->locations[$i]->{$weekdays[$j]})){
                     $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                    }
                }
            $locations .= '</ul>';
            $locations .= '</div>';
            $locations .= '<div class="direction">';
            $locations .= '<a href="https://www.google.com/maps/dir//'.urlencode($location_address).'" target="_blank" class="fl-button" role="button">
                            <span class="button-text">GET DIRECTIONS</span></a>';
            $locations .= '</div>';
            $locations .= '<div class="map-container">
            <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
            </div>';
        }
        else{

        
                if(in_array("loc",$arg)){
                    if(in_array(trim($location_name),$arg) || in_array("all",$arg)){
                        
                        $locations .= '<div class="store-container">';
                            //$locations .= '<div class="name">'.$location_name.'</div>';
                            $locations .= '<div class="address"><a href=https://www.google.com/maps/dir//'.urlencode($location_address).'" target="_blank" '.'>'.$location_address.'</div>';
                            $locations .= '<div class="phone"><a href=tel:'.$location_phone.'>'.'<span>'.$location_phone.'</span></a></div>';
                        $locations .= '</div>';
                    }
                }
                if(in_array("ohrs",$arg)){
                    if(in_array($location_name,$arg)  || in_array("all",$arg)){
                        $locations .= '<div class="store-opening-hrs-container">';
                        $locations .= '<ul class="store-opening-hrs">';
                        
                        for($j = 0; $j < count($weekdays); $j++) {
                            
                            $location .= $website->locations[$i]->monday;
                            if(isset($website->locations[$i]->{$weekdays[$j]})){
                                $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                            }
                        }
                        $locations .= '</ul>';
                        $locations .= '</div>';
                    }
                }
                if(in_array("dir",$arg)){
                    if(in_array($location_name,$arg)  || in_array("all",$arg)){
                        $locations .= '<div class="direction">';
                        $locations .= '<a href="https://www.google.com/maps/dir//'.urlencode($location_address).'" target="_blank" class="fl-button" role="button">
                                        <span class="button-text">GET DIRECTIONS</span></a>';
                        $locations .= '</div>';
                    }
                }
                if(in_array("map",$arg)){
                    if(in_array($location_name,$arg)  || in_array("all",$arg)){
                    $locations .= '<div class="map-container">
                            <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
                            </div>';
                            }
                        }
        }
            $locations .= '</li>';
            
            
            
        }
    $locations .= '</ul>';
    return $locations;
    }
    add_shortcode( 'storelocation_address', 'storelocation_address');

    function storelocation_map($arg){
        $website = json_decode(get_option('website_json'));
        
        for($i=0;$i<count($website->locations);$i++){
            
            $map = '<div class="map-container">
                <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.$website->locations[$i]->address.$website->locations[$i]->city.$website->locations[$i]->postalCode.$website->locations[$i]->state.'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
            </div>';
            return $map;
            
        }
    
    }
        add_shortcode( 'storelocation_map', 'storelocation_map' );

function contact_phonenumber( $atts ) {
    
    if(get_option('api_contact_phone'))
        return '<a href="tel:'.get_option('api_contact_phone').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">'.get_option('api_contact_phone').'</a>';
    else
        return '<a href="javascript:void(0)" target="_self" class="fl-icon-text-link fl-icon-text-wrap">(XXX) (XXXXXXX)</a>';
}
add_shortcode( 'contactPhnumber', 'contact_phonenumber' );


function contact_address_display( $atts ) {
    return '
    <div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
	<div class="">
    <div class="fl-icon-wrap">

	<span class="fl-icon">
								<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" tabindex="-1" aria-hidden="true" aria-labelledby="fl-icon-text-5be49d131d734">
							<i class="fas fa-map-marker" aria-hidden="true"></i>
				</a>
			</span>

		<div id="fl-icon-text-5be49d131d734" class="fl-icon-text">
				<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap">
				<p>'.get_option('api_contact_address_sandbox').'</p>				</a>
			</div>
</div>
</div></div>
<div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
	<div class="">
		<div class="fl-icon-wrap">

	<span class="fl-icon">
								<a href="tel:'.get_option('api_contact_phone').'" target="_blank" tabindex="-1" aria-hidden="true" aria-labelledby="fl-icon-text-5be49d131d770">
							<i class="fas fa-phone" aria-hidden="true"></i>
				</a>
			</span>

		<div id="fl-icon-text-5be49d131d770" class="fl-icon-text">
				<a href="tel:'.get_option('api_contact_phone').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap">
</a><p><a href="tel:'.get_option('api_contact_phone').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap"></a><a href="tel:'.get_option('api_contact_phone').'">'.get_option('api_contact_phone').'</a></p>
			</div>
	
</div>
	</div>
</div>'
;
}
add_shortcode( 'contactAddress', 'contact_address_display' );

https://www.google.com/maps/dir//Dalton+Wholesale+Floors,+411+Soho+Dr,+Adairsville,+GA+30103/@34.381066,-84.9075907,17z/data=!4m15!1m6!3m5!1s0x88f555733e5d7859:0x4a06216b9216e888!2sDalton+Wholesale+Floors!8m2!3d34.381066!4d-84.905402!4m7!1m0!1m5!1m1!1s0x88f555733e5d7859:0x4a06216b9216e888!2m2!1d-84.905402!2d34.381066
function getDirectionButton(){
    return '
		<div class="getdirection">
			<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" class="fl-button" role="button">
							<span class="fl-button-text">GET DIRECTIONS</span>
                    </a>
                    
</div>
	';
}
add_shortcode( 'getDirection', 'getDirectionButton' );

function getOpeningHours(){
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			'.get_option('api_opening_hr_fields_sandbox').'
                    
</div>
	';
}
add_shortcode( 'getOpeningHours', 'getOpeningHours' );

function getSocailIcons(){
    $details = json_decode(get_option('social_links'));
    $return  = '';
    if(isset($details)){
        $return  = '<ul class="social-icons">';
            foreach($details as $key => $value){
                if($value->platform=="googleBusiness"){
                    $value->platform = "google-plus";
                }
                if($value->platform=="linkedIn"){
                    $value->platform = "linkedin";
                }
                
                
                if($value->active)
                $return  .= '<li><a href="'.$value->url.'" target="_blank" title="'.$value->platform.'"><i class="fab fa-'.$value->platform.'"></i></a></li>';
            }
        $return  .= '</ul>';
        
    }
    return $return;
}
add_shortcode( 'getSocailIcons', 'getSocailIcons' );

function create_post_type() {
    register_post_type('store-locations',
      array(
        'labels' => array(
          'name' => __( 'Store Locations' ),
          'singular_name' => __( 'Store Location' )
        ),
        'public' => true,
        'has_archive' => true,
      )
    );
  }
add_action( 'init', 'create_post_type' );

function contactInformation( $atts ) {
    $contacts = json_decode(get_option('website_json'));
    
    if(is_array($contacts->contacts)){
        if(in_array("withicon",$atts )){
            for($i=0;$i<count($contacts->contacts);$i++){
                if(in_array($contacts->contacts[$i]->type,$atts )){
                    if(in_array("email",$atts )){
                        $info  .= "<a class='mail-icon icon-link' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if(in_array("phone",$atts )){
                        $info  .= "<a class='phone-icon icon-link' href='tel:".$contacts->contacts[$i]->phone."'>".$contacts->contacts[$i]->phone."</a>";
                    }
                    if(in_array("notes",$atts )){
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
        else{
            for($i=0;$i<count($contacts->contacts);$i++){
                if(in_array($contacts->contacts[$i]->type,$atts )){
                    if(in_array("email",$atts )){
                        $info  .= "<a class='mail-icon-with icon-link-with' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if(in_array("phone",$atts )){
                        $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".$contacts->contacts[$i]->phone."'>".$contacts->contacts[$i]->phone."</a>";
                    }
                    if(in_array("notes",$atts )){
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
    }
    return $info;
    
}
add_shortcode( 'contactInformation', 'contactInformation' );
